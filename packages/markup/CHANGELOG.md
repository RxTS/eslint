# eslint-plugin-markup

## 0.9.0

### Minor Changes

- [`2616f9d`](https://github.com/rx-ts/eslint/commit/2616f9dbf3fc81935f987907e56fa5af3f9266d3) Thanks [@JounQin](https://github.com/JounQin)! - chore(deps)!: bump all (dev)Dependencies, use exports field

### Patch Changes

- Updated dependencies [[`2616f9d`](https://github.com/rx-ts/eslint/commit/2616f9dbf3fc81935f987907e56fa5af3f9266d3)]:
  - eslint-plugin-utils@0.3.0

## 0.8.0

### Minor Changes

- [`a2c68dd`](https://github.com/rx-ts/eslint/commit/a2c68dd7ac4ff9b67b0cb1f371a008cf359dae19) Thanks [@JounQin](https://github.com/JounQin)! - feat: upgrade markuplint 2.0

### Patch Changes

- Updated dependencies [[`a2c68dd`](https://github.com/rx-ts/eslint/commit/a2c68dd7ac4ff9b67b0cb1f371a008cf359dae19)]:
  - eslint-plugin-utils@0.2.0

## 0.7.1

### Patch Changes

- [#33](https://github.com/rx-ts/eslint/pull/33) [`8f00e33`](https://github.com/rx-ts/eslint/commit/8f00e33dd66cb777264454f17197a984d922b75c) Thanks [@JounQin](https://github.com/JounQin)! - chore(deps): upgrade synckit v0.3.0

## 0.7.0

### Minor Changes

- [#32](https://github.com/rx-ts/eslint/pull/32) [`7d5dfce`](https://github.com/rx-ts/eslint/commit/7d5dfcefa8a813682d428d563e689e3d91344581) Thanks [@JounQin](https://github.com/JounQin)! - feat: call `creatSyncFn` lazily for performance

### Patch Changes

- [#29](https://github.com/rx-ts/eslint/pull/29) [`c7e7bd8`](https://github.com/rx-ts/eslint/commit/c7e7bd80746345219c3c14cc296910308b33c38c) Thanks [@JounQin](https://github.com/JounQin)! - feat(markup): upgrade synckit which is much faster now

## 0.6.1

### Patch Changes

- [`c8ff9fe`](https://github.com/rx-ts/eslint/commit/c8ff9fea0addc71cc0053a84cf6e8bc54900322c) Thanks [@JounQin](https://github.com/JounQin)! - fix: eslint column is 0-indexed

## 0.6.0

### Minor Changes

- [#22](https://github.com/rx-ts/eslint/pull/22) [`2270c8b`](https://github.com/rx-ts/eslint/commit/2270c8b0049725eb59bbc0cc45e9655b6162d735) Thanks [@JounQin](https://github.com/JounQin)! - feat: report correct ruleId from original linter

### Patch Changes

- Updated dependencies [[`2270c8b`](https://github.com/rx-ts/eslint/commit/2270c8b0049725eb59bbc0cc45e9655b6162d735)]:
  - eslint-plugin-utils@0.1.0

## 0.5.0

### Minor Changes

- [#20](https://github.com/rx-ts/eslint/pull/20) [`47b79a4`](https://github.com/rx-ts/eslint/commit/47b79a41cd2e328b3cfde5a7a03126197c8dd083) Thanks [@JounQin](https://github.com/JounQin)! - chore: bump all (dev)Dependencies

## 0.4.0

### Minor Changes

- [#9](https://github.com/rx-ts/eslint/pull/9) [`62b23ed`](https://github.com/rx-ts/eslint/commit/62b23ed0f90bd6bce4286099eec8c886ded7ff0e) Thanks [@JounQin](https://github.com/JounQin)! - feat: integrate with markuplint-sync

## 0.3.1

### Patch Changes

- [#7](https://github.com/rx-ts/eslint/pull/7) [`2927d23`](https://github.com/rx-ts/eslint/commit/2927d23eb674aa8af56400fc80637a4297754641) Thanks [@JounQin](https://github.com/JounQin)! - fix: bump synckit for better types

## 0.3.0

### Minor Changes

- [#5](https://github.com/rx-ts/eslint/pull/5) [`2bb9c39`](https://github.com/rx-ts/eslint/commit/2bb9c39ddab1000791a5030eb435a0702c015854) Thanks [@JounQin](https://github.com/JounQin)! - feat: use [`synckit`](https://github.com/rx-ts/synckit) to simplify

## 0.2.0

### Minor Changes

- [#3](https://github.com/rx-ts/eslint/pull/3) [`a6cf268`](https://github.com/rx-ts/eslint/commit/a6cf26816557eb47a96df4f6dadb5f1e1ed1e282) Thanks [@JounQin](https://github.com/JounQin)! - feat: support async markuplint API via child_process

## 0.1.0

### Minor Changes

- [`589ca12`](https://github.com/rx-ts/eslint/commit/589ca12548b4f5c31707a5679eb6d29c9e9a5c78) Thanks [@JounQin](https://github.com/JounQin)! - feat: first blood, should just work
